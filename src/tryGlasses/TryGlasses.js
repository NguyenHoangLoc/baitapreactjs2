import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
import Model from "./Model";
import Glasses from "./Glasses";
export default class TryGlasses extends Component {
  state = {
    choseGlasses: dataGlasses[0],
  };
  handleChoseGlasses = (glasses) => {
    this.setState({
      choseGlasses: glasses,
    });
  };
  renderGlassesList = () => {
    return dataGlasses.map((item) => {
      return (
        <Glasses
          handleClick={this.handleChoseGlasses}
          glasses={item}
          key={item.id.toString()}
        />
      );
    });
  };
  render() {
    return (
      <div>
        <Model glasses={this.state.choseGlasses} />
        <div className="row">{this.renderGlassesList()}</div>
      </div>
    );
  }
}
