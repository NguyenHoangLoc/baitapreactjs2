import React, { Component } from "react";
import style from "./Glasses.module.css";
export default class Glasses extends Component {
  render() {
    let { url } = this.props.glasses;
    return (
      <div className="col-2">
        <img
          className={style.glasses}
          onClick={() => {
            this.props.handleClick(this.props.glasses);
          }}
          src={url}
          alt=""
        />
      </div>
    );
  }
}
