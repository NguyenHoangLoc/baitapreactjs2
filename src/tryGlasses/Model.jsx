import React, { Component } from "react";
import model from "./glassesImage/model.jpg";
import style from "./Model.module.css";
export default class Model extends Component {
  render() {
    return (
      <>
        <div className={style.container}>
          <img src={model} alt="" />
          <div className={style.glassChose}>
            <img src={this.props.glasses.url} alt="" />
          </div>
          <div className={style.info}>
            <div className="text-left">
              <div className="d-flex justify-content-between">
                <h5>{this.props.glasses.name}</h5>
                <h5 className="mx-auto">${this.props.glasses.price}</h5>
              </div>
              <p className={style.desc}>{this.props.glasses.desc}</p>
            </div>
          </div>
        </div>

        <img className={style.container} src={model} alt="" />
      </>
    );
  }
}
