import "./App.css";
import TryGlasses from "./tryGlasses/TryGlasses";
function App() {
  return (
    <div className="App">
      <TryGlasses />
    </div>
  );
}

export default App;
